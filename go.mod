module library

go 1.17

require (
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.2.0
)

require github.com/gorilla/mux v1.8.0

require (
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible // indirect
	github.com/gorilla/schema v1.2.0 // indirect
)
